# A2.4
def convert():
    print("Welcome to Sejong's Korean Won Exchange Bank!")
    cur = input("Currency: ")
    amo = float(input("Amount: "))
    global gout
    gout = 0
    if cur in "EUR":
        gout=amo*1290.29
        print("EUR -->> ", 1290.29)
    elif cur in "USD":
        gout=amo*1200
        print("USD -->> ", 1200)
    elif cur in "GBP":
        gout=amo*1300
        print("GBP -->> ", 1300)
    elif cur in "JPY":
        gout=amo*60
        print("JPY -->> ", 60)
    else:
        print("Invalid value")

    global gfinal
    gfinal = 0
    global gif
    gif = 0
    if gout > 0:
        if gout < 100000:
            gif =  (gout / 100) * 5
            print("Conversion cost: ", gif)
        elif gout > 100000 and gout < 500000:
            gif = (gout / 100) * 4
            print("Conversion cost: ", gif)
        elif gout > 500000 and gout < 2000000:
            gif =(gout / 100) * 3
            print("Conversion cost: ", gif)
        elif gout > 2000000 and gout < 5000000:
            gif = (gout / 100) * 2
            print("Conversion cost: ", gif)
        elif gout > 5000000:
            gif = (gout / 100) * 1
            print("Conversion cost: ", gif)
    else:
        print("Invalid value")

    print("The amount: ", gout)

    print("Final amount: ", gout+gif)
    print("Thank you and please come again! ")



convert()